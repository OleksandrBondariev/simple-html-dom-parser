<?php

/**
 * Метод 
 * 
 * Вставка содержимого массива $out в таблицу MySQL
 * @param array
 */
function insert($out) {
    /**
     * Соединение с MySQL
     * 
     * Устанавливает новое соединение с сервером MySQL
     * "localhost" - хост 
     * "root"      - имя пользователя MySQL.
     * "9338922"   - пароль
     * "lv2_4"     - имя базы данных
     */
    $mysqli = new mysqli("localhost", "root", "9338922", "lv2_4");
    if ($mysqli->connect_errno) {
        die('Connect Error: ' . $mysqli->connect_errno);
        exit();
    }
    create($mysqli);
    /**
     * Подготавливает SQL выражение к выполнению
     * 
     * Подготовка SQL выражение для вставки в таблицу bills_ru_events
     */
    // $stmt = $mysqli->prepare("INSERT INTO bills_ru_events (date, title, url) VALUES (?, ?, ?)");
    $stmt = $mysqli->prepare("INSERT INTO bills_ru_events (date, title, url) VALUES (?, ?, ?)");
    /**
     * Привязка переменных к параметрам подготавливаемого запроса
     * 
     * Привязка переменных к параметрам подготавливаемого запроса
     * Соответствующие переменные имеют тип string
     */
    $stmt->bind_param('sss', $date, $title, $url);


    /**
     * Создаем массив
     * 
     * Опредиляем массив, где ключи короткое обозначение названия месяца на русском языке,
     * а значение его цифровое обозначение
     */
    $month = array(
        "янв" => 01,
        "фев" => 02,
        "мар" => 03,
        "апр" => 04,
        "мая" => 05,
        "июн" => 06,
        "июл" => 07,
        "авг" => 08,
        "сен" => 09,
        "окт" => 10,
        "ноя" => 11,
        "дек" => 12,
    );


    /**
     * Перебор массива
     * 
     * Изменение кодировки символов.
     * Изменение эллементов массива со значением буквенного определения месяца
     * на циферное
     */
    for ($i = 0; $i < count($out); $i++) {
     
        /**
         * Преобразует кодировку символов
         * 
         * Преобразует символы строки string $out[][] в кодировку UTF-8 
         * из windows-1251
         */
        $url = mb_convert_encoding($out[$i]['url'], 'UTF-8', 'windows-1251');
        $title = mb_convert_encoding($out[$i]['title'], 'UTF-8', 'windows-1251');
        $date = mb_convert_encoding($out[$i]['date'], 'UTF-8', 'windows-1251');
        
        
        
        preg_match("/\s*(\d{1,2})\s*(\w+)\s*/uis", $date, $matches);
        $date = date("Y") . '-' . $month[$matches[2]] . '-' . $matches[1];


        /**
         * Выполняем подготовленный запрос
         */
        $stmt->execute();
    }
    /* закрываем запрос */
    $stmt->close();
}

/**
 * Метод созания таблицы
 * 
 * 
 * Создание таблицы `bills_ru_events` имеющую такую структуру:
 * 
 * id       целое, автоинкрементарное
 * date     в формате год-месяц-день часы:минуты:секунды
 * title    строковое не более 230 символов
 * url      строковое не более 240 символов, уникальное
 */
function create($mysqli) {
    $create = "CREATE TABLE IF NOT EXISTS `bills_ru_events` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `date` datetime NOT NULL,
                `title` varchar(230) NOT NULL,
                `url` varchar(240) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `url` (`url`)
                ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;";


    /**
     * Выполняем подготовленный запрос к базе данных
     */
    $mysqli->query($create);
}
