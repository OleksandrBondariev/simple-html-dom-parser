
<?php
/**
 * Уровень 2, задача 4 
 * 
 * Cкрипт закачивания страницы www.bills.ru, из страницы извлекаются даты, 
 * заголовки, ссылки в блоке "события на долговом рынке", 
 * сохраняются таблицу bills_ru_events, имеющую такую структуру:
 * 
 * id       целое, автоинкрементарное
 * date     в формате год-месяц-день часы:минуты:секунды
 * title    строковое не более 230 символов
 * url      строковое не более 240 символов, уникальное
 * 
 * @version 1.1
 */

include 'MySQLconnection.php';
include('simple_html_dom.php');

// Create DOM from URL
$html = file_get_html("http://www.bills.ru");


    /**
     * Поиск эллементов html
     * 
     * Находим все контейнеры с классом "bizon_api_news_row" в таблице 
     * с классом "bizon_api_list bizon_api_news_table" 
     * и созраняем в ассоциативный массив:
     * 'date'   - дата
     * 'title'  - описание ссылки
     * 'url'    - URL, ccылка
     */

foreach($html->find('table[class=bizon_api_list bizon_api_news_table]',0)->find('tr[class=bizon_api_news_row]') as $article) {
    $item['date'] = $article->find('td.news_date', 0)->plaintext;
    $item['title'] = $article->find('a',0)->plaintext;
    $item['url'] = $article->find('a',0)->href;

    $articles[] = $item;
}

/**
 * 
 * Вызов функции insert($out);
 * 
 * Вставка содержимого массива  в    MySQL
 */
insert($articles);